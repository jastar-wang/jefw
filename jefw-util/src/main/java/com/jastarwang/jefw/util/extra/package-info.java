/**
 * 扩展工具类模块<br>
 * 该包用于存放对第三方库依赖或支持的工具类；使用该包的工具类时，需注意引用其所需依赖，详情请查看相关文档。
 *
 * @author Jastar Wang
 * @since 1.0
 */
package com.jastarwang.jefw.util.extra;