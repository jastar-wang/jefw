package com.jastarwang.jefw;

/**
 * 欢迎使用 <code>jefw</code>
 *
 * @author Jastar Wang
 * @date 2020/10/17
 * @since 1.0
 */
public class Welcome {
    public static void main(String[] args) {
        System.out.println("Hello , Jefw!");
    }
}
