# 更新日志

## 2.0.0 - 2025-03-11（jdk 17+）

### 特性

- 【all】重要：从`2.0.0`开始，全面升级至`SpringBoot 3.x`，`JDK`最低要求`17+`；支持`JDK 8`的老版本已归档至`1.x`
  分支，仅作紧急`bug`修复，不再添加新特性。

---

## 1.3.3 - 2025-02-26

### 依赖

- 【all】升级三方库依赖版本

---

## 1.3.2 - 2024-09-28

### 依赖

- 【all】升级三方库依赖版本，以解决部分CVE漏洞

---

## 1.3.1 - 2023-05-10

### 特性

- 【oss】新增：方法`getConfig`，更便捷、动态的获取存储配置
- 【oss】新增：方法`listObj`，支持根据前缀查询对象列表的功能
- 【oss】新增：方法`getPrivateUrl`重载，支持预签名地址使用官方域名或自定义域名
- 【oss】新增：操作模板类`JefwOssTemplate`中加入装饰方法，支持使用默认服务及其配置调用方法

### 优化

- 【oss】优化：方法`prototype`重命名为`getClient`，更能见名知意（不兼容）

---

## 1.3.0 - 2023-04-09（JitPack正式版）

> Tips：重大更新，建议基于此版本开始使用。

### 特性

- 【core】新增：`ResponseResult`支持修改默认响应信息
- 【core】新增：`ResponseResult`支持格式化输出的方法
- 【sms】新增：短信服务模块，支持同时存在多个实现，并提供统一的操作模板类
- 【oss】重构：支持同时存在多个对象存储实现，并提供统一的操作模板类（不兼容）

### 优化

- 【all】优化：类、变量、方法等定义（部分不兼容）

### 依赖

- 【all】升级`spring-boot`版本（`2.7.6` => `2.7.10`）
- 【all】升级`jackson`版本（`2.14.1` => `2.14.2`）
- 【oss】升级`aliyun.sdk.oss`（`3.13.2` => `3.16.2`）
- 【oss】升级`tencent.sdk.cos`（`5.6.123` => `5.6.139`）
- 【util】升级`hutool`版本（`5.8.10` => `5.8.16`）
- 【sms】新增`tencent.cloud.sdk`依赖（`3.1.728`）

---

## [1.2.4] - 2023-02-20

### 特性

- 【oss】重构：以bean的形式重新实现对象存储服务，而非工具类，并添加腾讯云厂商实现（不兼容）
- 【redis】重构：添加core包依赖，调整分布式锁包结构，修改锁接口函数参数（不兼容），并新增无返回值接口

---

## [1.2.3] - 2023-01-07

### 特性

- 【core】新增：无参数无返回值的函数式接口`Execute`
- 【redis】新增：分布式锁不阻塞等待的接口

### 优化

- 【redis】优化：加锁失败异常类的命名（不兼容）
- 【redis】优化：分布式锁参数的详细注释
- 【redis】优化：`RedissonClient`注入方式改为构造方法
- 【redis】优化：去除`RedissonClient`的自动配置，因为大多数情况下都需要自主配置，或直接引入`redisson-spring-boot-starter`

### 删除

- 【jwt】删除`JwtUtil`工具类（`hutool`已在5.7.0引入实现）
- 【redis】删除`jedis`客户端依赖，因为`spring-boot`默认自带了`lettuce`

### 依赖

- 【all】升级`spring-boot`版本（`2.7.5` => `2.7.6`）

---

## [1.2.2] - 2022-11-24

### 特性

- 【core】新增：枚举工具类`Enums`重载方法
- 【util】新增：请求工具类`WebUtil`重载方法
- 【redis】新增：`RedisUtil`重载方法

### 修复

- 【redis】修复：Jackson序列化默认不支持Java8时间类型的问题

### 优化

- 【core】枚举接口`Enumable`加入泛型以解决实现方法时出现警告的问题（不兼容）
- 【core】枚举工具`Enums`优化方法命名以达到见名知意（不兼容）

### 依赖

- 【all】升级`spring-boot`版本（`2.5.6` => `2.7.5`）
- 【all】升级`jackson`版本（`2.13.0` => `2.14.1`）
- 【redis】升级`jedis`版本（`3.6.3` => `4.3.1`）
- 【redis】升级`redisson`版本（`3.16.4` => `3.18.0`）
- 【util】升级`hutool`版本（`5.7.16` => `5.8.10`）

---

## [1.2.1] - 2022-04-15（JitPack预览版）

### 修复

- 【core】修复部分文档及注释描述错误
- 【oss】修复阿里云客户端销毁时未关闭的问题

### 优化

- 【all】全面升级`Junit4`至`Junit5`的单元测试

### 依赖

- 【all】升级`spring-boot`版本（`2.5.3` => `2.5.6`）
- 【oss】升级`aliyun-sdk-oss`版本（`3.13.1` => `3.13.2`）
- 【redis】升级`jedis`版本（`3.3.0` => `3.6.3`）
- 【redis】升级`redisson`版本（`3.16.1` => `3.16.4`）
- 【util】升级`hutool`版本（`5.7.5` => `5.7.16`）

---

## [1.2.0] - 2021-09-16

### 特性

- 【oss】新增:`AliyunOssUtil`工具类，目前仅支持阿里云
- 【jwt】新增：拆分出`jwt`独立模块，以规范设计扩展

### 优化

- 【all】规范`optional`和`provided`的使用场景

### 删除

- 【util】删除`jwt`的相关信息，修复错误设计

---

## [1.1.3-beta] - 2021-09-07

### 特性

- 【core】`Enums`工具类支持通过`value`反射获取

### 修复

- 【util】修复因删除`SpringUtil`工具类而导致自动注入失败的问题

---

## [1.1.2-beta] - 2021-08-23

### 优化

- 【core】枚举增强功能拆分出独立工具类`Enums`
- 【redis】优化并补充部分封装的方法

### 删除

- 【util】删除`SpringUtil`工具类（`hutool`已在5.1引入实现）

---

## [1.1.1-beta] - 2021-08-10

### 特性

- 【core】新增：`ResponseResult`重载方法，放开自定义参数限制

### 优化

- 【core】优化：`ResponseCode`重命名，符合接口命名规范
- 【core】优化：`ResponseCode`默认状态码及返回信息重新定义

---

## [1.1.0-beta] - 2021-07-28

### 特性

- 【core】新增：枚举扩展能力接口

### 依赖

- 【all】升级`spring-boot`版本（`2.3.1.RELEASE` => `2.5.3`）
- 【all】升级`hutool`版本（`5.4.2` => `5.7.5`）
- 【all】升级`redisson`版本（`3.13.6` => `3.16.1`）

---

## [1.0.0-alpha] - 2020-11-20

问世！

---

```
## [version] - yyyy-MM-dd
### 特性
- 【模块】新增XXX
### 修复
- 【模块】修复XXX（issue/pr#编号@Git平台）
### 优化
- 【模块】优化XXX
### 依赖
- 【模块】升级XXX到XXX版本
### 过期
- 【模块】过期XXX
### 删除
- 【模块】删除XXX
### 贡献者
感谢以下所有为这个版本作出贡献的人：
- 张三
- 李四
```